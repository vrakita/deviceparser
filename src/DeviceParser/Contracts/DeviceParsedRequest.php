<?php 

namespace DeviceParser\Contracts;

interface DeviceParsedRequest {

	/**
	 * Parses (split header and body) request string
	 *
	 * @param string $content
	 * @return \DeviceHandler\Parser
	 * @throws \Exception
	 */
	public static function parse($content);


	/**
	 * Return header associative array
	 *
	 * @return array
	 */
	public function getHeader();


	/**
	 * Return events associative array
	 *
	 * @return array
	 */
	public function getEvents();

}

