<?php

namespace DeviceParser;

use DeviceParser\Contracts\DeviceParsedRequest;

class Parser implements DeviceParsedRequest {

	/**
	 * Content for parsing
	 *
	 * @var string
	 */
	protected $content;

	/**
	 * Header (parsed) of sent signal
	 * 
	 * @var array
	 */
	protected $header;

	/**
	 * Event data (parsed)
	 *
	 * @var array
	 */
	protected $events;

	/**
	 * Prevents class to be manually instantiated
	 *
	 */
	protected function __construct(){}


	/**
	 * Removes empty strings from array
	 *
	 * @param array $array
	 * @param mixed $removeCharacter
	 */
	protected function cleanArray($array, $removeCharacter = false) {

		
		if($removeCharacter) {

			$array = array_map(function($item) use($removeCharacter) {

				return str_replace($removeCharacter, "", $item);

			}, $array);

		}

		$cleaned = array_filter($array, function($item) {
			
			return strlen(trim($item)) > 0 ? trim($item) : false;

		});		

		return $cleaned;

	}

	/**
	 * Separates parsed content on header and body / events
	 *
	 * @param string $content
	 */
	protected function splitBodyAndHeader($content) {

		return $this->cleanArray(explode("\r", $content));

	}

	/**
	 * Parse string
	 *
	 * @param string $content
	 * @return void
	 * @throws \Exception
	 *
	 */
	public static function parse($content) {	

		if( ! $content) throw new \Exception('File do not exist');

		try {

			$parser = new self;

			$parser->content = $parser->splitBodyAndHeader($content);

			$parser->parseHeader();

			$parser->parseEvents();

			return $parser;

		} catch(\Exception $e) {

			throw new \Exception($e->getMessage());

		}		

	}

	/**
	 * Make array of header
	 * 
	 * @throws \Exception
	 * @return void
	 */
	protected function parseHeader() {

		if( ! is_array($this->content) || count($this->content) != 2) throw new \Exception('Parsed content not valid');

		$headerArray = $this->cleanArray($this->makeArray($this->content[0], "]"), "[");

		foreach ($headerArray as $value) {

			// If we are exploding string by : it will mess up our date, 
			// so we need condition if current field is date
			$pair = explode(":", $value);

			if(strtolower($pair[0]) == 'dt') {

				$new 	= explode(" ", $value);

				$break 	= explode(":", $new[0]);

				$key 	= $break[0];

				$date 	= $break[1];

				$string = $date . ' ' . $new[1];

				$date = \DateTime::createFromFormat('d.m.y H:i:s', $string);

				$this->header[strtolower($key)] = $date->format('Y-m-d H:i:s');

			} else {				
				// It is not date, so we can get second part of the string

				if( count($pair) != 2) throw new \Exception('Parsed content not valid. Header cannot be parsed');

				$this->header[strtolower($pair[0])] = $pair[1];
			}		

		}	

	}

	/**
	 * Makes array of string, explode by passed delimiter
	 *
	 * @return array
	 */
	protected function makeArray($string, $delimiter) {

		return explode($delimiter, $string);

	}

	/**
	 * Make array for events
	 * 
	 * @throws \Exception
	 * @return void
	 */
	protected function parseEvents() {

		if( ! is_array($this->content) || count($this->content) != 2) throw new \Exception('Parsed content not valid. Events cannot be parsed');

		$content = $this->cleanArray($this->makeArray($this->content[1], ";"), ["[", "]", "EV:"]);

		foreach ($content as $key => $value) {
			
			$data = $this->makeArray($value, "/");

			$this->events[] = $this->createEvent($data);

		}

	}

	/**
	 * Return header associative array
	 *
	 * @return array
	 */
	public function getHeader() {

		return $this->header;

	}

	/**
	 * Return events associative array
	 *
	 * @return array
	 */
	public function getEvents() {

		return $this->events;

	}

	/**
	 * Creates associative array
	 *
	 * @param array
	 * @return array
	 */
	protected function createEvent($array) {		

		if( count($array) !== 3) throw new \Exception('Parsed content not valid. Events cannot be created');

		$time = strtotime($this->header['dt']) + $array[0];

		return [
			'time' 	=> date('Y-m-d H:i:s', $time),
			'event' => $array[1],
			'value' => $array[2]
		];

	}

	public static function parseBTS($bts){

		if( strlen($bts) == 13 ){

			$mcc = substr($bts, 0, 3);
			$mnc = substr($bts, 3, 2);
			$lac = substr($bts, 5, 4);
			$cellid = substr($bts, 9, 4);

		}else if(strlen($bts) == 14 ){

			$mcc = substr($bts, 0, 3);
			$mnc = substr($bts, 3, 3);
			$lac = substr($bts, 6, 4);
			$cellid = substr($bts, 10, 4);

		}else {
			throw new \Exception('Parsed BTS not valid. BTS wrong length');
		}

		if (!ctype_digit ( $mcc ) 	||
			!ctype_digit ( $mnc ) 	||
			!ctype_xdigit ( $lac) 	||
			!ctype_xdigit ( $cellid )) throw new \Exception('Parsed BTS not valid. Parsed data wrong type');

		return[
			'mcc' => $mcc,
			'mnc' => $mnc,
			'lac' => hexdec($lac),
			'cellid' => hexdec($cellid)
		];

	}
}