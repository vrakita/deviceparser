<?php

use DeviceParser\Parser;

class ParserTest extends PHPUnit_Framework_TestCase {

	protected $long;
	protected $medium;
	protected $invalid1;
	protected $invalid2;
	protected $parameters = ['mcc', 'mnc', 'lac', 'cellid'];

	protected $parser;

	protected function setUp() {

		$this->long 	= file_get_contents('./tests/files/long.txt');
		// $this->medium 	= file_get_contents('./tests/files/medium.txt');
		$this->invalid1 = file_get_contents('./tests/files/invalid-1.txt');
		$this->invalid2 = file_get_contents('./tests/files/invalid-2.txt');

	}

	public function test_parsing_header() {

		// [in] => 868325020177434
	    // [dt] => 2016-06-28 12:25:53
	    // [catos] => 662
	    // [bat] => 55

	    $parser = Parser::parse($this->long);

		$array = $parser->getHeader();

		$this->assertTrue(is_array($array) &&  ! empty($array));

		$this->assertTrue(isset($array['dt']));
		$this->assertEquals('2016-06-28 12:25:53', $array['dt']);

		$date = \DateTime::createFromFormat('Y-m-d H:i:s', $array['dt']);

		$this->assertEquals($date->format('Y-m-d H:i:s'), $array['dt']);

		$this->assertTrue(isset($array['in']));
		$this->assertEquals('868325020177434', $array['in']);

		$this->assertTrue(isset($array['catos']));
		$this->assertEquals('662', $array['catos']);

		$this->assertTrue(isset($array['bat']));
		$this->assertEquals('55', $array['bat']);

	}

	public function test_parsing_events() {

		// parsed $array[1] from ./tests/files/long.txt 
		// [time] => 2016-06-28 20:04:24
        // [event] => BTS
        // [value] => 2200100D274CD

        $parser = Parser::parse($this->long);

		$array = $parser->getEvents();

		$this->assertTrue(is_array($array) &&  ! empty($array));

		$this->assertTrue(isset($array[1]['time']));
		$this->assertEquals('2016-06-28 20:04:24', $array[1]['time']);

		$date = \DateTime::createFromFormat('Y-m-d H:i:s', $array[1]['time']);

		$this->assertEquals($date->format('Y-m-d H:i:s'), $array[1]['time']);

		$this->assertTrue(isset($array[1]['event']));
		$this->assertEquals('BTS', $array[1]['event']);

		$this->assertTrue(isset($array[1]['value']));
		$this->assertEquals('2200100D274CD', $array[1]['value']);
		
		
	}

	
	public function test_parsing_wrong_formatted_file() {

		$message1 = '';
		$message2 = '';
		$message3 = '';

		try {

			Parser::parse($this->invalid1);	

		} catch(\Exception $e) {

			$message1 = $e->getMessage();

		}
		

		try {

			Parser::parse($this->invalid2);	

		} catch(\Exception $e) {

			$message2 = $e->getMessage();

		}


		try {

			@$content = file_get_contents(uniqid() . '.txt');

			Parser::parse($content);	

		} catch(\Exception $e) {

			$message3 = $e->getMessage();

		}

		$this->assertEquals('File do not exist', $message3);
		$this->assertEquals('Parsed content not valid. Events cannot be created', $message2);
		$this->assertEquals('Parsed content not valid. Header cannot be parsed', $message1);


	}

	public function test_parsing_valid_bts() {

		$array = Parser::parseBTS('220015208B82B');

		$this->assertEquals('220', $array['mcc']);
		$this->assertEquals('01', $array['mnc']);
		$this->assertEquals('21000', $array['lac']);
		$this->assertEquals('47147', $array['cellid']);

	}

	public function test_parsing_invalid_bts() {

		$message = '-';

		try {

			Parser::parseBTS('220015208B82B55');

		} catch(\Exception $e) {
			$message = $e->getMessage();
		}

		$this->assertEquals('Parsed BTS not valid. BTS wrong length', $message);

	}

	public function test_parse_returning_all_parameters_for_13_characters() {				

		$array = Parser::parseBTS('2200100D274CD');

		foreach ($this->parameters as $parameter) {
			
			$this->assertTrue(isset($array[$parameter]));

		}

	}

	public function test_parse_returning_all_parameters_for_14_characters() {				

		$array = Parser::parseBTS('22001500D274CD');

		foreach ($this->parameters as $parameter) {
			
			$this->assertTrue(isset($array[$parameter]));

		}

	}

}